use std::any::Any;
use std::collections::HashMap;
use std::fmt::Display;
use std::hash::Hash;
use serde::Serialize;
use serde_with::serde_as;
use serde_with::FromInto;
use log::error;
use thiserror::Error;
use crate::topology::fallback::FallbackGenerator;
use crate::topology::{K3STopologyKey, K3STopologyGenerator};

mod topology;
mod serializer;

#[serde_as]
#[derive(Serialize)]
struct K3SMainConfig<LK: Display>{
    // kube_proxy_arg: Vec<String>,
    #[serde(rename = "node-label+", serialize_with = "serializer::serialize_k3s_config_value")]
    node_label: HashMap<LK, String>,
}

impl<LK: Display> Default for K3SMainConfig<LK> {
    fn default() -> Self {
        todo!()
    }
}


fn main() {
    let generators: Vec<Box<dyn K3STopologyGenerator>> = vec![
        // Box::new(OCIInstanceMetadataClient::default()),
        Box::new(FallbackGenerator::default()),
    ];

    let topology = 'tloop: loop {
        for generator in generators {
            match generator.k3s_topology() {
                Ok(t) => break 'tloop t,
                Err(e) => log::error!("{e}"),
            }
        }
        panic!("No generator was able to produce a config")
    };

    let config = K3SMainConfig{
        node_label: topology,
    };

    let yaml = serde_yaml::to_string(&config).expect("Unable to encode to yaml");
    println!("{yaml}");
}
