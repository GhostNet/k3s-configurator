use std::str::FromStr;
use std::time::Duration;
use reqwest::blocking::{Client, ClientBuilder};
use reqwest::header::{AUTHORIZATION, HeaderMap, HeaderValue};
use reqwest::{StatusCode, Url};
use serde::Deserialize;
use crate::topology::{K3STopologyKey, K3STopologyGenerator, K3STopologyGeneratorError};

const URL: &str = "http://169.254.169.254/opc/v2/instance/";
const AUTH: &str = "Bearer Oracle";

#[derive(Deserialize)]
struct OCIRegionInfo {
    realm_key: String,
}

#[derive(Deserialize)]
struct OCIInstanceMetadata {
    oci_ad_name: String,
    region: String,
    region_info: OCIRegionInfo,
}

pub struct OCIInstanceMetadataClient {
    client: Client,
}

impl Default for OCIInstanceMetadataClient {
    fn default() -> Self {
        let mut headers = HeaderMap::new();
        headers.append(AUTHORIZATION, HeaderValue::from_str(AUTH).unwrap());

        let builder = ClientBuilder::new()
            .default_headers(headers)
            .connect_timeout(Duration::from_secs(1));

        Self {
            client: builder.build().unwrap()
        }
    }
}

impl K3STopologyGenerator for OCIInstanceMetadataClient {
    fn k3s_topology(&self) -> Result<Vec<K3STopologyKey>, K3STopologyGeneratorError> {
        let req = self.client.get(Url::from_str(URL).unwrap()).build().unwrap();
        let res = self.client.execute(req).map_err(|e| {
            match e {
                _ if e.is_connect() => {
                    K3STopologyGeneratorError::InvalidMethod
                }
                _ if e.is_status() => match e.status().unwrap() {
                    StatusCode::NOT_FOUND => K3STopologyGeneratorError::InvalidMethod,
                    _ => K3STopologyGeneratorError::UnknownError
                }
                _ => K3STopologyGeneratorError::UnknownError,
            }
        })?
            .json::<OCIInstanceMetadata>().unwrap();

        Ok(vec![
            K3STopologyKey::Region(format!("{}-{}", res.region_info.realm_key, res.region)),
            K3STopologyKey::Zone(res.oci_ad_name),
        ])
    }
}
