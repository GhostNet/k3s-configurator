use std::collections::HashMap;
use std::fmt::{Display, Formatter, Pointer, Write};
use serde::Serialize;
use strum_macros::Display;
use thiserror::Error;

// pub mod oci;
pub mod fallback;

#[strum(prefix= "topology.kubernetes.io/", serialize_all = "snake_case")]
#[derive(Eq, PartialEq, Hash, Display)]
pub enum K3STopologyKey {
    Region,
    Zone,
}


#[derive(Debug, Error)]
pub enum K3STopologyGeneratorError {
    #[error("This method is not valid for this instance")]
    InvalidMethod,
    #[error("An unknown error has occured")]
    UnknownError,
}

pub(crate) trait K3STopologyGenerator {
    fn k3s_topology(&self) -> Result<HashMap<K3STopologyKey, String>, K3STopologyGeneratorError>;
}
