use std::collections::HashMap;
use std::io::Read;
use std::net::IpAddr;
use std::str::FromStr;
use hickory_resolver::config::{ResolverConfig, ResolverOpts};
use hickory_resolver::error::{ResolveError, ResolveErrorKind};
use hickory_resolver::Resolver;
use crate::topology::{K3STopologyKey, K3STopologyGenerator, K3STopologyGeneratorError};

const AS_DOMAIN: &str = ".origin.asn.cymru.com";

pub struct FallbackGenerator {
    resolver: Resolver,
}

impl Default for FallbackGenerator {
    fn default() -> Self {
        let mut opts = ResolverOpts::default();
        opts.validate = true;
        let resolver = Resolver::new(
            ResolverConfig::cloudflare(),
            ResolverOpts::default(),
        ).unwrap();
        Self {
            resolver
        }
    }
}

impl K3STopologyGenerator for FallbackGenerator {
    fn k3s_topology(&self) -> Result<HashMap<K3STopologyKey, String>, K3STopologyGeneratorError> {
        let res = reqwest::blocking::get("https://ipv4.canhazip.com")
            .unwrap()
            .text()
            .unwrap();

        let ip = IpAddr::from_str(res.trim()).unwrap();

        let reverse_lookup = self.resolver.reverse_lookup(ip)
            .map_err(|e| {
                log::error!("{e}");
                K3STopologyGeneratorError::UnknownError
            })?;

        let zone = match reverse_lookup.iter().find(|ptr| !ptr.is_empty()) {
            Some(ptr) => ptr.to_string().trim_end_matches(".").to_string(),
            None => ip.to_string(),
        };

        let ptr_domain = reverse_lookup.query().name().to_string();
        let as_domain = ptr_domain.replace(".in-addr.arpa", AS_DOMAIN);

        let region = match self.resolver.txt_lookup(as_domain.clone()) {
            Ok(txt) => {
                Ok(match txt.iter().find(|txt| !txt.to_string().is_empty()) {
                    Some(txt) => format!("AS{}", txt.to_string().split_once(" | ").unwrap().0),
                    None => as_domain.clone(),
                })
            }
            Err(e) => {
                match e.kind() {
                    ResolveErrorKind::NoRecordsFound { .. } => {
                        Ok(as_domain.clone())
                    }
                    _ => Err(K3STopologyGeneratorError::UnknownError)
                }
            }
        }?;

        let mut map = HashMap::with_capacity(2);
        map.insert(K3STopologyKey::Region, region);
        map.insert(K3STopologyKey::Zone, zone);
        Ok(map)
    }
}
