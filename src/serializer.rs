use std::collections::HashMap;
use std::fmt::Display;
use std::ops::Deref;
use serde::{Serialize, Serializer};

pub fn serialize_k3s_config_value<S, K, V>(value: &HashMap<K, V>, s: S) -> Result<S::Ok, S::Error>
    where S: Serializer, K: Display, V: Display {
    let list: Vec<String> = value.into_iter().map(|(k, v)| {
        format!("{k}={v}")
    })
        .collect();

    list.serialize(s)
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;
    use std::fmt::{Display, Formatter};
    use serde::Serialize;
    use serde_with::StringWithSeparator;

    #[test]
    fn test_serialize_label() {
        #[strum(prefix= "test.label/", serialize_all = "snake_case")]
        #[derive(Eq, PartialEq, Hash, strum_macros::Display)]
        enum Labels {
            Foo,
            Bar,
        }


        #[derive(Serialize)]
        struct Parent<LK: Display> {
            #[serde(serialize_with = "super::serialize_k3s_config_value")]
            labels: HashMap<LK, String>,
        }

        let mut map = HashMap::new();
        map.insert(Labels::Foo, "foo-label".to_string());
        map.insert(Labels::Bar, "bar-label".to_string());

        let parent = Parent {
            labels: map,
        };

        let output = serde_yaml::to_string(&parent).unwrap();
        print!("{output}")
    }
}
