#!/bin/bash

apt-get update

if [[ $TARGETPLATFORM == "linux/amd64" ]]; then
  RUST_TARGET="x86_64-unknown-linux-musl"
elif [[ $TARGETPLATFORM == "linux/arm64" ]]; then
  RUST_TARGET="aarch64-unknown-linux-musl"
  apt install -y gcc-aarch64-linux-gnu
else
  echo "WARNING: unknown target, compiling natively. This may not be the desired behaviour"
  exit 0
fi

apt-get install musl-tools -y

/usr/local/cargo/bin/rustup target add $RUST_TARGET

cargo build --release --target ${RUST_TARGET}
cargo install --path . --verbose --target ${RUST_TARGET}
