# syntax=docker/dockerfile:1

FROM --platform=$BUILDPLATFORM rust as build
LABEL authors="ghost"
ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /usr/src/app

COPY . .

RUN --mount=type=cache,target=/usr/local/cargo/registry,id=${TARGETPLATFORM} --mount=type=cache,target=/usr/src/app/target,id=${TARGETPLATFORM} \
    ./ci/build.sh
RUN echo $TARGETPLATFORM | sed -E "s/.*\//ARCHITECTURE=$1/g" >> systemd/extension-release.k3s-configurator

FROM scratch

COPY --from=build /usr/local/cargo/bin/k3s-configurator /usr/bin/k3s-configurator
COPY --from=build /usr/src/app/systemd /usr/lib/extension-release.d
